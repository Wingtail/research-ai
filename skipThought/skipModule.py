import skipthoughts
import re
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin_min
from nltk.tokenize import sent_tokenize

def loadModule():
    model = skipthoughts.load_model()
    return model

def read_article(fN):
    file = open(fN,"r")
    filedata = file.readlines()
    sentences = []
    for data in filedata:
        article = re.sub(r'[^a-z A-Z.]','',data)
        sentences.extend(sent_tokenize(article))
        for j in reversed(range(len(sentences))):
            sent = sentences[j]
            sentences[j] = sent.strip()
            if sent == '':
                sentences.pop(j)

        #for sentence in article:
        #    if('?' not in sentence):
        #        sent = sentence.split(' ')
        #        if(len(sent) > 1):
        #            sentences.append(sent)
        #sentences.pop()

    return sentences

def encodeSentences(model, sentences):
    encoder = skipthoughts.Encoder(model)
    encoded = encoder.encode(sentences, verbose=False)
    return encoded

def summarize(encodedMatrix, sentences, nClusters=6):
    kmeans = KMeans(n_clusters=nClusters, random_state=0)
    kmeans = kmeans.fit(encodedMatrix)
    avg=[]
    closest=[]

    for i in range(nClusters):
        arr = np.where(kmeans.labels_ == i)[0]
        avg.append(np.mean(arr))

    closest, _ = pairwise_distances_argmin_min(kmeans.cluster_centers_,\
                                                   encodedMatrix)
    #assuming that the sentences are in order --> not mixed

    ordering = sorted(range(nClusters), key=lambda k: avg[k])

    summary = ' '.join([sentences[closest[idx]] for idx in ordering])

    return summary

def summarizeArticle(model, fileName, count):
    sentences = read_article(fileName)
    encodedMatrix = encodeSentences(model, sentences)
    return summarize(encodedMatrix, sentences, count)
