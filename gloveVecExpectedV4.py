from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
import numpy as np
import networkx as nx
import re
import math

module = { }

def loadGlove(gloveFile):
    print 'Loading Glove Module'
    f = open(gloveFile,'r')

    for line in f:
        splitLine = line.split()
        embedding = np.array([float(val) for val in splitLine[1:]])
        module[splitLine[0]] = embedding

    print module['for']

def read_article(fN):
    file = open(fN,"r")
    filedata = file.readlines()
    sentences = []
    for data in filedata:
        article = re.sub(r'[^a-z A-Z.]','',data).split('. ')
        for sentence in article:
            if('?' not in sentence):
                sent = sentence.split(' ')
                if(len(sent) > 1):
                    sentences.append(sent)
        #sentences.pop()

    return sentences

def iqr(arr):
    arr.sort()
    print arr
    print len(arr)
    q3 = arr[int(float((len(arr)-1)/4*3))] #Q3
    q1 = arr[int(float((len(arr)-1)/4))] #Q1

    i = q3-q1

    iqrH = (1.5*i)+q3
    iqrL = q1-(1.5*i)
    return [iqrH, iqrL]

def sentenceSimilarity(sent1, sent2, stopword=None):
    if stopword is None:
        stopword=[]
    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    sent1 = [w for w in sent1 if w not in stopword]
    sent2 = [w for w in sent2 if w not in stopword]

    s = []

    for word1 in sent1:
        temp = []
        for word2 in sent2:
            if(word1 in module and word2 in module):
                temp.append(1-cosine_distance(module[word1], module[word2]))
        if(len(temp) > 1):
            IQR = iqr(temp)
            count = 0
            sum = float(0)
            for val in temp:
                if(val <= IQR[0] and val >= IQR[1]):
                    sum += val
                    count += 1

            s.append(sum/count)

    for word2 in sent2:
        temp = []
        for word1 in sent1:
            if(word1 in module and word2 in module):
                temp.append(1-cosine_distance(module[word2],module[word1]))
        if(len(temp)>1):
            IQR = iqr(temp)
            count = 0
            sum = float(0)
            for val in temp:
                if(val <= IQR[0] and val >= IQR[1]):
                    sum += val
                    count += 1

            s.append(sum/count)

    s.sort()

    #Compute IQR

    IQR = iqr(s)
    sum = float(0)
    count = 0
    for su in s:
        if(su<IQR[0] and su > IQR[1]):
            sum+=su
            count+=1
    sum/=count

    #since 0 means similar we want 1 to indicate maximum similarity
    print('cosine similarity: ', sum)
    return sum

def buildSimilarityMatrix(sentences, stopWords):
    similarMatrix = np.zeros((len(sentences), len(sentences))) #square matrix out of sentences length
    for id1 in range(len(sentences)):
        for id2 in range(len(sentences)):
            if(id1 == id2):
                continue
            similarMatrix[id1][id2] = sentenceSimilarity(sentences[id1], sentences[id2], stopWords)

    print similarMatrix
    return similarMatrix

def summarize(fN, glovePath, importance=5):
    loadGlove(glovePath)
    stop_words = stopwords.words('english')
    summaryText = []

    sentences = read_article(fN)
    similarMatrix = buildSimilarityMatrix(sentences, stop_words)

    similarityGraph = nx.from_numpy_array(similarMatrix)
    scores = nx.pagerank(similarityGraph)

    print ('scores: ', scores)

    rankedSentences = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)

    print("Ranked sentences: ", rankedSentences)

    for i in range(importance):
        summaryText.append(' '.join(rankedSentences[i][1]))

    print ('. '.join(summaryText))


