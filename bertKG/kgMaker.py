import stanfordnlp
nlp = stanfordnlp.Pipeline()
sentence = "Biology is the natural science that studies life and living organisms, including their physical structure, chemical processes, molecular interactions, physiological mechanisms, development and evolution."
doc = nlp(sentence)

#Relationship is the fundamental unit of knowledge graph
#Modeled synthetically through stanford sentence dependency parsing

class Relationship:
    def __init__(self, node1, node2, relationship):
        self.relationship = relationship #is Node class
        self.node1 = node1
        self.node2 = node2

        existence = self.relationship.searchDependency('cop')
        if(existence != None):
            self.node2 = self.relationship
            self.relationship = existence

        if(self.node2 == None):
            self.node2 = self.relationship.searchDependency('obl')
        else:
            if(self.node2=='obl'):
                self.relationship.name = relationship.name+"_"+self.node2.searchDependency('case')[0].name


        self.node1.getAttributes()
        self.node2.getAttributes()
    def searchDependency(self, dependency):
        que = []
        #from node's connections, search for the dependency
        for connection in self.relationship.connections:
            if(connection[1].find(dependency) > -1):
                return connection[0]
        return None
    def getBranches(self):
        relationships = []
        if(self.node1.getRelationships() != None):
            relationships.extend(self.node1.getRelationships())
        if(self.node2.getRelationships() != None):
            relationships.extend(self.node2.getRelationships())
        return relationships

    def relation(self):
        return self.node1.name+" __ "+self.relationship.name+" __ "+self.node2.name


class Node:
    def __init__(self, name, id, dependency):
        self.connections = [] #dependency parse
        self.name = name
        self.id = id
        self.dependency = dependency
        self.attributes = []
        self.relationships = []
        
    def indent(num):
        for i in range(num-1):
            print('     ', end="")
        print('---- ', end="")
        return
    
    def printAllConnections(self, attr, count):
        indent(count)
        print(self.name, " : ", attr)
        count += 1
        for connection in self.connections:
            connection[0].printAllConnections(connection[1], count)
        
        return connections
    
    def searchDependency(self, dependency):
        que = []
        #from node's connections, search for the dependency
        for connection in self.connections:
            if(connection[1].find(dependency) > -1):
                return connection[0]
        return None
    
    def searchDependencies(self,dependencies):
        dependencys = []
        for connection in self.connections:
            for dependency in dependencies:
                if(connection[1].find(dependency) > -1):
                    dependencys.append(connection[0])
        return [i for i in dependencys if i != None]
    
    def getAttributes(self):
        attribute = self.searchDependency('amod')
        if attribute not in self.attributes:
            self.attributes.append(attribute)
            
    def getRelationships(self):
        relationship = []
        
        depend = self.searchDependencies(['acl:relcl','conj','obj','nmod','nsubj','expl','obl'])
        if(len(depend)>0):
            relationship.extend(depend)
                
        
        node2 = None
        
        relations = []
        
        if(len(relationship) > 0): #searching list of dependencies
            for relation in relationship:
                node2 = []
                node2.extend(relation.searchDependencies('obj'))
                node2.extend(relation.searchDependencies('conj'))
                node2.extend(relation.searchDependencies('nmod'))
                node2.extend(relation.searchDependencies('obl'))
                
                if(relation.dependency == 'conj'):
                        for rel in self.relationships:
                                if(rel.node2 is self):
                                    r = Relationship(rel.node1, relation, rel.relationship)
                                    relations.append(r)
                                    relation.relationships.append(r)

                if(len(node2) < 1):
                    temp = relation.searchDependency('cop')
                    if(temp != None):
                        node2 = relation
                        relationship = temp

                        node2.getAttributes()
                        
                        rel = Relationship(self,node2,relation)
                        node2.relationships.append(rel)
                        relations.append(rel)
                        return rel
                    else:
                        node1 = self.searchDependency('expl')
                        node2 = None
                        if(node1 == None):
                            node1 = self.searchDependency('nsubj')
                            node2 = self.searchDependency('obj')
                        else:
                            node2 = self.searchDependency('nsubj')
                        if(node1 != None and node2 != None):
                            r = Relationship(node1, node2, self)
                            node1.relationships.append(r)
                            node2.relationships.append(r)
                            relations.append(r)
                    
                
                else:
                    for node in node2:
                        if(node.dependency == 'conj'):
                            for rel in relation.relationships:
                                if(rel.node2 is relation):
                                    r = Relationship(rel.node1, node, rel.relationship)
                                    relations.append(r)
                                    node.relationships.append(r)
                                elif(rel.relationship.dependency == 'case'):
                                    if(rel.node1 is relation):
                                        r = Relationship(rel.node1, node, rel.relationship)
                                        relations.append(r)
                                        node.relationships.append(r)
                        elif(node.dependency == 'case'):
                            r = Relationship(self, relation, node)
                            node.relationships.append(r)
                            relations.append(r)
                        elif(node.dependency == 'nmod' or node.dependency == 'obl'):
                            rel = node.searchDependency('case')
                            if(rel != None):
                                r = Relationship(relation,node,rel)
                                relations.append(r)
                                node.relationships.append(r)
                        elif(node.dependency=='obj'):
                            r = Relationship(self, node, relation)
                            node.relationships.append(r)
                            relations.append(r)
                
            
        else:
            return None
        
        if(len(relations)>0):
            self.relationships.extend(relations)
        else: #it is some sort of root relationship
            node1 = self.searchDependency('expl')
            node2 = None
            if(node1 == None):
                node1 = self.searchDependency('nsubj')
                node2 = self.searchDependency('obj')
            else:
                node2 = self.searchDependency('nsubj')
            if(node1 != None and node2 != None):
                r = Relationship(node1, node2, self)
                node1.relationships.append(r)
                node2.relationships.append(r)
                relations.append(r)
        
        return relations
    
    

#Converting dependency parsing data into node representation
nodes = []
dependencies = []
nodes.append(Node('ROOT', 0, ""))
for dependency in doc.sentences[0].dependencies:
    nodes.append(Node(dependency[2].text, dependency[2].index, dependency[2].dependency_relation))

for dependency in doc.sentences[0].dependencies:
    parent = None
    child = None
    for node in nodes:
        if(int(node.id)==int(dependency[2].governor)):
            parent = node
        if(int(node.id)==int(dependency[2].index)):
            child = node
        if(parent != None and child != None):
            parent.connections.append((child, dependency[2].dependency_relation))
            break

#Use printAllConnections feature on root node to visualize entire sentence dependency parsing

#nodes[0].printAllConnections("",0)



#knowledge graph builder

que = []

que.append(nodes[0])

#searching for root, nsubj, obj
root = None
node1 = None
node2 = None

#for each sentence
root = que[0].searchDependency('root')
print(que[0].name)
node1 = root.searchDependency('nsubj')
node2 = root.searchDependency('obj')

relationships = []

relationships.append(Relationship(node1, node2, root))

i=0
while(i < len(relationships)):
    relationships.extend(relationships[i].getBranches())
    removeList = []
    for j in range(len(relationships)):
        for k in range(j+1,len(relationships)):
            if(relationships[j].relation() == relationships[k].relation()):
                removeList.append(relationships[k])
    for remov in removeList: #Need to fix redundant addition
        if(remov in relationships):
            relationships.remove(remov)
    i += 1

for relationship in relationships:
    print(relationship.relation())
