from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
import numpy as np
import networkx as nx
import re
import pandas as pd
import csv

def read_article(fN):
    file = open(fN,"r")
    filedata = file.readlines()
    sentences = []
    for data in filedata:
        article = re.sub(r'[^a-z A-Z.]','',data).split('. ')
        for sentence in article:
            if('?' not in sentence):
                sent = sentence.split(' ')
                if(len(sent) > 1):
                    sentences.append(sent)
        #sentences.pop()


    return sentences

def sentenceSimilarity(sent1, sent2, stopword=None):
    if stopword is None:
        stopword=[]
    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    allWords = list(set(sent1+sent2))

    vector1=[0]*len(allWords)
    vector2=[0]*len(allWords)

    for w in sent1:
        if w in stopword:
            continue
        vector1[allWords.index(w)] += 1

    for w in sent2:
        if w in stopword:
            continue
        vector2[allWords.index(w)] += 1

    #since 0 means similar we want 1 to indicate maximum similarity

    return 1-cosine_distance(vector1, vector2)

def buildSimilarityMatrix(sentences, stopWords):
    similarMatrix = np.zeros((len(sentences), len(sentences))) #square matrix out of sentences length
    for id1 in range(len(sentences)):
        for id2 in range(len(sentences)):
            if(id1 == id2):
                continue
            similarMatrix[id1][id2] = sentenceSimilarity(sentences[id1], sentences[id2], stopWords)

    print similarMatrix
    return similarMatrix

def summarize(fN, importance=5):
    stop_words = stopwords.words('english')
    summaryText = []

    sentences = read_article(fN)
    similarMatrix = buildSimilarityMatrix(sentences, stop_words)

    similarityGraph = nx.from_numpy_array(similarMatrix)
    scores = nx.pagerank(similarityGraph)

    print ('scores: ', scores)

    rankedSentences = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)

    print("Ranked sentences: ", rankedSentences)

    for i in range(importance):
        summaryText.append(' '.join(rankedSentences[i][1]))

    print ('. '.join(summaryText))


