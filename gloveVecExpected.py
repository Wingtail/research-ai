from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
import numpy as np
import networkx as nx
import re

module = { }

def loadGlove(gloveFile):
    print 'Loading Glove Module'
    f = open(gloveFile,'r')

    for line in f:
        splitLine = line.split()
        embedding = np.array([float(val) for val in splitLine[1:]])
        module[splitLine[0]] = embedding

    print module['for']

def read_article(fN):
    file = open(fN,"r")
    filedata = file.readlines()
    sentences = []
    for data in filedata:
        article = re.sub(r'[^a-z A-Z.]','',data).split('. ')
        for sentence in article:
            if('?' not in sentence):
                sent = sentence.split(' ')
                if(len(sent) > 1):
                    sentences.append(sent)
        #sentences.pop()

    print sentences
    return sentences

def computeFrequency(sentences, stopword):
    allWords = {}
    val = 0
    for sentence in sentences:
        sentence = [w.lower() for w in sentence]
        sentence = [w for w in sentence if w not in stopword]
        for word in sentence:
            val += 1
            if word in allWords:
                allWords[word] += 1
            else:
                allWords[word] = float(1)

    for word in allWords:
        allWords[word] /= val

    print('wordFreqDist: ', allWords)
    return allWords

def computeExpectedVector(sentence, wordFreqDist):
    E = np.zeros(1)
    for word in sentence:
        if(word in module and word in wordFreqDist):
            exp = wordFreqDist[word]*module[word]
            E = np.add(E, exp)
    print sentence
    print('E: ', E)
    return E

def sentenceSimilarity(sent1, sent2, wordFreqDist, stopword=None):
    if stopword is None:
        stopword=[]
    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    sent1 = [w for w in sent1 if w not in stopword]
    sent2 = [w for w in sent2 if w not in stopword]

    vector1 = computeExpectedVector(sent1, wordFreqDist)
    vector2 = computeExpectedVector(sent2, wordFreqDist)
    #since 0 means similar we want 1 to indicate maximum similarity
    print('cosine similarity: ', 1-cosine_distance(vector1, vector2))
    return 1-cosine_distance(vector1, vector2)

def buildSimilarityMatrix(sentences, stopWords):
    similarMatrix = np.zeros((len(sentences), len(sentences))) #square matrix out of sentences length
    wordFreqDist = computeFrequency(sentences, stopWords)
    for id1 in range(len(sentences)):
        for id2 in range(len(sentences)):
            if(id1 == id2):
                continue
            similarMatrix[id1][id2] = sentenceSimilarity(sentences[id1], sentences[id2], wordFreqDist, stopWords)

    print similarMatrix
    return similarMatrix

def summarize(fN, glovePath, importance=5):
    loadGlove(glovePath)
    stop_words = stopwords.words('english')
    summaryText = []

    sentences = read_article(fN)
    similarMatrix = buildSimilarityMatrix(sentences, stop_words)

    similarityGraph = nx.from_numpy_array(similarMatrix)
    scores = nx.pagerank(similarityGraph)

    print ('scores: ', scores)

    rankedSentences = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)

    print("Ranked sentences: ", rankedSentences)

    for i in range(importance):
        summaryText.append(' '.join(rankedSentences[i][1]))

    print ('. '.join(summaryText))


